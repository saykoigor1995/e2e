from selenium import webdriver
import time
        
capabilities = {
    "browserName": "chrome",
    "browserVersion": "96.0",
    "selenoid:options": {
        "enableVNC": True,
        "enableVideo": False
    }
}

driver = webdriver.Remote(
    command_executor="http://localhost:4444/wd/hub",
    desired_capabilities=capabilities)

try:
    print ('Session ID is: %s' % driver.session_id)
    print ('Oppening the page...')
    driver.get('https://www.citilink.ru/')
    time.sleep(5)

    burger = driver.find_element_by_xpath('/html/body/div[2]/div[2]/header/div[2]/div[1]/div[2]/a/button').click()
    time.sleep(1)

    searchbox = driver.find_element_by_xpath('/html/body/div[2]/div[2]/header/div[3]/div/div/div/div/div/menu/div/div[1]/div[1]/div/form/div/div[1]/div/label/input').send_keys('iphone')

    searchsvg = driver.find_element_by_xpath('/html/body/div[2]/div[2]/header/div[3]/div/div/div/div/div/menu/div/div[1]/div[1]/div/form/div/div[1]/div/label/span[2]/span[2]/button').click()

    time.sleep(2)
    filtermin = driver.find_element_by_xpath('//*[@id="app-filter"]/div/div/div/div[2]/div/div[3]/div[2]/div[2]/input[1]').send_keys('10000')
finally:
    driver.quit()
